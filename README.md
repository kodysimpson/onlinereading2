# online-reading
Read. Online. Anytime.

This website is for reading! Thats it for now!
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
  </button>
                <h4 class="modal-title" id="myModalLabel"><%= allbooks.name %></h4>
            </div>
            <!--Body-->
            <div class="modal-body">
                <%= allbooks.desc %>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <a href="/read/<%= allbooks.id %>"><button type="button" class="btn btn-primary">Read Book</button></a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
