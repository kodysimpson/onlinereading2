var mongoose = require("mongoose");
 //BOOK SCHEMA
var bookSchema = new mongoose.Schema({
   isbn: String,
   name: String,
   author: String,
   genre : Array,
   imageName: String,
   fileName: String,
   desc: String,
   fiction: String,
   date: String,
   poster: String,
   rating: String,
   pageCount: String
});
module.exports = mongoose.model("Books", bookSchema);
// module.exports = mongoose.model("PrivateBooks", bookSchema);
