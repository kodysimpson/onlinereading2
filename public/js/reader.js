EPUBJS.Hooks.register("beforeChapterDisplay").endnotes = function(a, b) {
    var c = b.contents.querySelectorAll("a[href]"),
        d = Array.prototype.slice.call(c),
        e = EPUBJS.core.folder(location.pathname),
        f = (EPUBJS.cssPath, {});
    EPUBJS.core.addCss(EPUBJS.cssPath + "popup.css", !1, b.render.document.head), d.forEach(function(a) {
        function c() {
            var c, h, n = b.height,
                o = b.width,
                p = 225;
            m || (c = j.cloneNode(!0), m = c.querySelector("p")), f[i] || (f[i] = document.createElement("div"), f[i].setAttribute("class", "popup"), pop_content = document.createElement("div"), f[i].appendChild(pop_content), pop_content.appendChild(m), pop_content.setAttribute("class", "pop_content"), b.render.document.body.appendChild(f[i]), f[i].addEventListener("mouseover", d, !1), f[i].addEventListener("mouseout", e, !1), b.on("renderer:pageChanged", g, this), b.on("renderer:pageChanged", e, this)), c = f[i], h = a.getBoundingClientRect(), k = h.left, l = h.top, c.classList.add("show"), popRect = c.getBoundingClientRect(), c.style.left = k - popRect.width / 2 + "px", c.style.top = l + "px", p > n / 2.5 && (p = n / 2.5, pop_content.style.maxHeight = p + "px"), popRect.height + l >= n - 25 ? (c.style.top = l - popRect.height + "px", c.classList.add("above")) : c.classList.remove("above"), k - popRect.width <= 0 ? (c.style.left = k + "px", c.classList.add("left")) : c.classList.remove("left"), k + popRect.width / 2 >= o ? (c.style.left = k - 300 + "px", popRect = c.getBoundingClientRect(), c.style.left = k - popRect.width + "px", popRect.height + l >= n - 25 ? (c.style.top = l - popRect.height + "px", c.classList.add("above")) : c.classList.remove("above"), c.classList.add("right")) : c.classList.remove("right")
        }

        function d() {
            f[i].classList.add("on")
        }

        function e() {
            f[i].classList.remove("on")
        }

        function g() {
            setTimeout(function() {
                f[i].classList.remove("show")
            }, 100)
        }
        var h, i, j, k, l, m;
        "noteref" == a.getAttribute("epub:type") && (h = a.getAttribute("href"), i = h.replace("#", ""), j = b.render.document.getElementById(i), a.addEventListener("mouseover", c, !1), a.addEventListener("mouseout", g, !1))
    }), a && a()
}, EPUBJS.Hooks.register("beforeChapterDisplay").mathml = function(a, b) {
    if (b.currentChapter.manifestProperties.indexOf("mathml") !== -1) {
        b.render.iframe.contentWindow.mathmlCallback = a;
        var c = document.createElement("script");
        c.type = "text/x-mathjax-config", c.innerHTML = '        MathJax.Hub.Register.StartupHook("End",function () {           window.mathmlCallback();         });        MathJax.Hub.Config({jax: ["input/TeX","input/MathML","output/SVG"],extensions: ["tex2jax.js","mml2jax.js","MathEvents.js"],TeX: {extensions: ["noErrors.js","noUndefined.js","autoload-all.js"]},MathMenu: {showRenderer: false},menuSettings: {zoom: "Click"},messageStyle: "none"});                 ', b.doc.body.appendChild(c), EPUBJS.core.addScript("http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML", null, b.doc.head)
    } else a && a()
}, EPUBJS.Hooks.register("beforeChapterDisplay").smartimages = function(a, b) {
    var c = b.contents.querySelectorAll("img"),
        d = Array.prototype.slice.call(c),
        e = b.height;
    if ("reflowable" != b.layoutSettings.layout) return void a();
    d.forEach(function(a) {
        var c = function() {
                var c, d = a.getBoundingClientRect(),
                    f = d.height,
                    g = d.top,
                    h = a.getAttribute("data-height"),
                    i = h || f,
                    j = Number(getComputedStyle(a, "").fontSize.match(/(\d*(\.\d*)?)px/)[1]),
                    k = j ? j / 2 : 0;
                e = b.contents.clientHeight, g < 0 && (g = 0), i + g >= e ? (g < e / 2 ? (c = e - g - k, a.style.maxHeight = c + "px", a.style.width = "auto") : (i > e && (a.style.maxHeight = e + "px", a.style.width = "auto", d = a.getBoundingClientRect(), i = d.height), a.style.display = "block", a.style.WebkitColumnBreakBefore = "always", a.style.breakBefore = "column"), a.setAttribute("data-height", c)) : (a.style.removeProperty("max-height"), a.style.removeProperty("margin-top"))
            },
            d = function() {
                b.off("renderer:resized", c), b.off("renderer:chapterUnload", this)
            };
        a.addEventListener("load", c, !1), b.on("renderer:resized", c), b.on("renderer:chapterUnload", d), c()
    }), a && a()
}, EPUBJS.Hooks.register("beforeChapterDisplay").transculsions = function(a, b) {
    var c = b.contents.querySelectorAll("[transclusion]");
    Array.prototype.slice.call(c).forEach(function(a) {
        function c() {
            j = g, k = h, j > chapter.colWidth && (d = chapter.colWidth / j, j = chapter.colWidth, k *= d), f.width = j, f.height = k
        }
        var d, e = a.getAttribute("ref"),
            f = document.createElement("iframe"),
            g = a.getAttribute("width"),
            h = a.getAttribute("height"),
            i = a.parentNode,
            j = g,
            k = h;
        c(), b.listenUntil("renderer:resized", "renderer:chapterUnloaded", c), f.src = e, i.replaceChild(f, a)
    }), a && a()
};
