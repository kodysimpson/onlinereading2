var express = require("express");
var router = express.Router();
var User = require("../models/user");
var Books = require("../models/book");
var TempBooks = require("../models/tempbook");
var PrivateBooks = require("../models/pbook");
var EPub = require("epub");
var libgen = require("libgen");
var isbn = require("node-isbn");
/////////////////////////////////////////////////////////////////////////////////////
var totalBooks;
Books.count({}, function(err, count) {
    if (err) {
        console.log("User count failed to load.");
    }
    else {
        totalBooks = count;
    }
});
var fiction;
Books.count({
    "fiction": "Fiction"
}, function(err, count) {
    if (err) {
        console.log("User count failed to load.");
    }
    else {
        fiction = count;
    }
});
var nonfiction;
Books.count({
    "fiction": "Nonfiction"
}, function(err, count) {
    if (err) {
        console.log("User count failed to load.");
    }
    else {
        nonfiction = count;
    }
});
var horror;
Books.count({
    "genre": "Horror"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        horror = count;
    }
});
var drama;
Books.count({
    "genre": "Drama"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        drama = count;
    }
});
var romance;
Books.count({
    "genre": "Romance"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        romance = count;
    }
});
var fantasy;
Books.count({
    "genre": "Fantasy"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        fantasy = count;
    }
});
var poetry;
Books.count({
    "genre": "Poetry"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        poetry = count;
    }
});
var comedy;
Books.count({
    "genre": "Comedy"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        comedy = count;
    }
});
var youngadult;
Books.count({
    "genre": "Young Adult"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        youngadult = count;
    }
});
var sciencefiction;
Books.count({
    "genre": "Science Fiction"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        sciencefiction = count;
    }
});
var mystery;
Books.count({
    "genre": "Mystery"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        mystery = count;
    }
});
var comics;
Books.count({
    "genre": "Comics"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        comics = count;
    }
});
var history;
Books.count({
    "genre": "History"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        history = count;
    }
});
var historicalfiction;
Books.count({
    "genre": "Historical Fiction"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        historicalfiction = count;
    }
});
var philosophy;
Books.count({
    "genre": "Philosophy"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        philosophy = count;
    }
});
var actionadventure;
Books.count({
    "genre": "Action/Adventure"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        actionadventure = count;
    }
});
var satire;
Books.count({
    "genre": "Satire"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        satire = count;
    }
});
var children;
Books.count({
    "genre": "Children"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        children = count;
    }
});
var religion;
Books.count({
    "genre": "Religion"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        religion = count;
    }
});
var biography;
Books.count({
    "genre": "Biography"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        biography = count;
    }
});
var autobiography;
Books.count({
    "genre": "Autobiography"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        autobiography = count;
    }
});
var art;
Books.count({
    "genre": "Art"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        art = count;
    }
});
var cooking;
Books.count({
    "genre": "Cooking"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    }
    else {
        cooking = count;
    }
});
// var science;
// Books.count({
//     "genre": "Science"
// }, function(err, count) {
//     if (err) {
//         console.log("Count failed.");
//     } else {
//         science = count;
//     }
// });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Genre Listing Route
router.get("/genres", function(req, res) {
    res.render("genres", {
        horror: horror,
        fiction: fiction,
        nonfiction: nonfiction,
        drama: drama,
        fantasy: fantasy,
        romance: romance,
        comedy: comedy,
        poetry: poetry,
        youngadult: youngadult,
        sciencefiction: sciencefiction,
        mystery: mystery,
        comics: comics,
        history: history,
        historicalfiction: historicalfiction,
        philosophy: philosophy,
        actionadventure: actionadventure,
        satire: satire,
        children: children,
        religion: religion,
        biography: biography,
        autobiography: autobiography,
        art: art,
        cooking: cooking
    });
});
router.get("/genres/:genre", function(req, res) {
    var genre = req.params.genre;
    Books.find({ "genre": genre }, function(err, books) {
        if (err) {
            console.log("You broke something. Fool.");
        }
        else {
            res.render("genrebooks", {
                books: books,
                genre: genre
            });
        }
    });
});
router.get("/fnf/:genre", function(req, res) {
    var genre = req.params.genre;
    Books.find({ "fiction": genre }, function(err, books) {
        if (err) {
            console.log(err);
        }
        else {
            res.render("genrebooks", {
                books: books,
                genre: genre
            });
        }
    });
});

//Find All Authors
router.get("/allauthors", function(req, res) {
    Books.find("author", function(err, books) {
        if (err) {
            console.log(err);
        }
        else {
            res.render("authors", {
                authorbooks: books
            });
        }
    });
});

//Search Author for his Books
router.get("/author/:author", function(req, res) {
    Books.find({"author": req.params.author}, function(err, books) {
        if (err) {
            console.log(err);
        }
        else {
            res.render("authorpage", {
                authorbooks: books
            });
        }
    });
});

//All Books 1 page
router.get("/all", function(req, res) {
    var pageNumber = 1;
    var pageTotal = Math.round(totalBooks / 16);
    var numDown = pageNumber - 1;
    var numUp = pageNumber + 1;
    Books.find({}, function(err, allbooks) {
        if (err) {
            console.log("Problem getting all books");
        }
        else {
            res.render("all", {
                allbooks: allbooks,
                num: pageNumber,
                numDown: numDown,
                numUp: numUp,
                pageTotal: pageTotal
            });
        }
    });
    //.limit(16);
});
//All Books Paginator
router.get("/all/:page", function(req, res) {
    //Get All books from DB
    var pageNumber = req.params.page;
    var pageTotal = Math.round(totalBooks / 16);
    var numDown = pageNumber - 1;
    var numUp = pageNumber + 1;
    if (req.params.size > 1) {
        Books.find({}, function(err, allbooks) {
            if (err) {
                console.log("Problem getting all books");
            }
            else {
                res.render("all", {
                    allbooks: allbooks,
                    num: pageNumber,
                    numDown: numDown,
                    numUp: numUp,
                    pageTotal: pageTotal
                });
            }
        }).skip(16 * (pageNumber - 1)).limit(16);
    }
    else {
        res.redirect("/all");
    }

});
//Upload Book Route
router.get("/upload", isLoggedIn, function(req, res) {
    res.render("upload");
});

//Revised Add Book Route
router.get("/addbook2/:id", function(req, res) {
            // TempBooks.findById(req.params.id, function(err, foundBook) {
            //     if (err) {
            //         console.log("Couldnt Find the Tempbook Correctly.");
            //     }
            //     else {
            //         var epub = new EPub("public/bib/bookshelf/" + foundBook.fileName);
            //         //"https://onlinereading-illuminatiiiiii.c9users.io/uploads/covers/" + imageName
            //         epub.on("end", function() {
            //             // epub is now usable
            //             //PARSER CORE
            //             console.log("Title: " + epub.metadata.title);
            //             console.log("Author: " + epub.metadata.creator);
            //             console.log("Description: " + epub.metadata.description);
            //             console.log("Genre: " + epub.metadata.subject);
            //             res.render("addbook", {
            //                 title: epub.metadata.title,
            //                 author: epub.metadata.creator,
            //                 desc: epub.metadata.creator,
            //                 genre: epub.metadata.subject
            //             });
            //         });
            //         epub.parse();
            //     }
            // });
            isbn.resolve(req.params.id, function(err, book) {
                    if (err) {
                        console.log('Book not found', err);
                        res.render("addbook", {
                                isbn: req.params.id,
                                title: "",
                                desc: "",
                                genre: "",
                                author: "",
                                imageLink: "",
                                rating: "",
                                pageCount: " "
                            });
                        }
                    else {
                        console.log('Book found %j', book);
                        if(book.readingModes.image == false){
                            res.render("addbook", {
                                title: book.title,
                                author: book.authors,
                                desc: book.description,
                                genre: book.categories,
                                isbn: req.params.id,
                                imageLink: "nothing",
                                rating: book.averageRating,
                                pageCount: book.pageCount
                            });
                        }else{
                             res.render("addbook", {
                                title: book.title,
                                author: book.authors,
                                desc: book.description,
                                genre: book.categories,
                                isbn: req.params.id,
                                imageLink: book.imageLinks.thumbnail,
                                rating: book.averageRating,
                                pageCount: book.pageCount
                            });
                        }
                        }
                        
                    });
            });


        //Modern Cortex Reader
        // router.get("/reader", function(req, res){
        // 	res.render("reader");
        // });
        //Normal Read
        router.get("/read/:id", function(req, res) {
            Books.findById(req.params.id, function(err, foundBook) {
                if (err) {
                    console.log("Something didnt work correctly");
                }
                else {
                    res.render("reader", {
                        book: foundBook,
                        id: req.params.id
                    });
                }
            });
        });
        //Private Read
        router.get("/privateread/:id", function(req, res) {
            PrivateBooks.findById(req.params.id, function(err, foundBook) {
                if (err) {
                    console.log("Something didnt work correctly");
                }
                else {
                    res.render("reader", {
                        book: foundBook,
                        id: req.params.id
                    });
                }
            });
        }); router.get("/read/:id/edit", function(req, res) {
            res.render("edit");
        });
        //Delete A Book Route
        router.get("/remove/:id", function(req, res) {
            Books.findByIdAndRemove(req.params.id, function(err) {
                if (err) {
                    console.log("There was a problem trying to delete a book.");
                }
            });
            res.redirect("/all");
        });
        //Get Random Book

        // router.get("/random", function(req, res) {
        //     Books.findOne({fiction: "fiction"}, function(err, allbooks) {
        //         if (err) {
        //             console.log("CRAP");
        //         } else {
        //             // res.render("book", {
        //             //     book: allbooks
        //             // });
        //             res.redirect("/read/" + allbooks.id);
        //         }
        //     });
        // });

        //Search Libgen Database
        const options = {
            mirror: 'http://libgen.io',
            query: "goosebumps",
            sort_by: 'title',
            reverse: false
        }; router.get("/lib", function(req, res) {
            //Do Search
            libgen.search(options, (err, data) => {
                if (err)
                    return console.error(err);
                let n = data.length;
                console.log(n + ' most recently published "' +
                    options.query + '" books');
                while (n--) {
                    console.log('***********');
                    console.log('Title: ' + data[n].title);
                    console.log('Author: ' + data[n].author);
                    console.log('Download: ' +
                        'http://libgen.io/book/index.php?md5=' +
                        data[n].md5.toLowerCase());
                }
            });
            const md5 = '6fd5a023fe6c792cac478bd4bc571190';
            libgen.utils.check.canDownload(md5, (err, url) => {
                if (err)
                    return console.error(err);
                return console.log('Working link: ' + url);
            });
        });

        function isLoggedIn(req, res, next) {
            if (req.isAuthenticated()) {
                return next();
            }
            res.redirect("/login");
        }
        module.exports = router;
