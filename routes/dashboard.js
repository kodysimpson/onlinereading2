var express = require("express");
var router = express.Router();
var User = require("../models/user");
var Books = require("../models/book");
var PrivateBooks = require("../models/pbook");


router.get("/dashboard", isLoggedIn, function(req, res){
    res.render("dashboard");
});

router.get("/pbookshelf/user/:username", isLoggedIn, function(req, res){
    PrivateBooks.find({"poster": req.params.username}, function(err, allbooks) {
        if (err) {
            console.log("Problem getting all books");
        } else {
            res.render("shelf", {
                allbooks: allbooks
          });
        }
    })
});

router.get("/bookshelf/user/:username", isLoggedIn, function(req, res){
  Books.find({}, function(err, allbooks) {
      if (err) {
          console.log("Problem getting all books");
      } else {
          res.render("shelf", {
              allbooks: allbooks
        });
      }
  })
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect("/login");
}
module.exports = router;
